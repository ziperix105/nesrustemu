mod nes;


fn main() {
    let mut nes = nes::create_vm();
    let rom = nes::rom::create_rom(String::from("tests/games/mario.nes"));

    nes.load_rom(&rom);
}
