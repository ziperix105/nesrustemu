pub mod rom;
mod ppu;
mod cpu;
mod memory;


pub struct NES {
    pub memory: memory::RAM,
    pub cpu: cpu::CPU,
    pub ppu: ppu::PPU,
}

pub fn create_vm() -> NES {
    NES {
        memory: memory::create_memory(),
        cpu:    cpu::create_cpu(),
        ppu:    ppu::create_ppu(),
    }
}

impl NES {
    pub fn load_rom(&mut self, rom: &rom::ROM) {
    
    }
}
