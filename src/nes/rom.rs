use std::io;
use std::io::prelude::*;
use std::fs::File;


#[allow(non_camel_case_types)]
enum ROMFormat {
    legacy_ines,
    ines07,
    ines,
    ines2,
}

pub struct ROM {
    path: String,
    header: [u8; 16],
    content: Vec<u8>,
}

impl ROM {
    fn load_data(&mut self) -> io::Result<()> {
        let mut file = File::open(self.path.to_owned())?;

        /* read header */
        file.read(&mut self.header)?;

        /* read rest of the file */
        file.read_to_end(&mut self.content)?;

        Ok(())
    }
}

pub fn create_rom(path: String) -> ROM {
    let mut rom = ROM {
        path,
        header: [0; 16],
        content: Vec::new(),
    };

    let io_res = rom.load_data();

    match io_res {
        Err(error) => panic!("Problem opening the file: {:?}", error),
        _ => (),
    }

    rom
}
