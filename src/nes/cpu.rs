pub struct CPU {
    pub a:  u8,
    pub x:  u8,
    pub y:  u8,
    pub s:  u8,
    pub p:  u8,
    pub pc: u16,
}

pub fn create_cpu() -> CPU {
    CPU {
        a: 0, x: 0, y: 0, s: 0, p: 0, pc: 0,
    }
}
