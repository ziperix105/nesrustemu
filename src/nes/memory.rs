pub struct RAM {
    content: [u8; 0xFFFF],
}

/* Constructor */
pub fn create_memory() -> RAM {
    RAM {
        content: [0; 0xFFFF],
    }
}

impl RAM {
    pub fn write_to_mem(&mut self, adress: u16, value: u8) {
        self.content[adress as usize] = value;
    }

    pub fn read_memory(&self, adress: u16) -> u8 {
        self.content[adress as usize]
    }
}
